package com.gearbox.platform.deadline.service;

import com.gearbox.core.model.Job;
import com.gearbox.core.service.JobService;
import com.gearbox.platform.deadline.exception.MethodNotSupportException;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@ConditionalOnProperty(name = "system.type", havingValue = "deadline")
public class DeadlineJobService implements JobService {
    @Override
    public List<Job> listWaitingJobs() {
        List<String> jobIds = DeadlineCommandExecutor.listWaitingJobIds();
        return jobIds.stream().map(this::getJobDetail).collect(Collectors.toList());
    }

    private Job getJobDetail(String jobId) {
        Job job = new Job();
        job.setId(jobId);
        job.setUser(DeadlineCommandExecutor.getJobUserName(jobId));
        job.setTaskCount(DeadlineCommandExecutor.getJobQueuedChunks(jobId));
        return job;
    }

    @Override
    public List<Job> listRunningJobs() {
        throw new MethodNotSupportException("deadline not support list running jobs");
    }
}
