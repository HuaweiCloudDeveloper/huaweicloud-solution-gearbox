package com.gearbox.platform.slurm.cache;

import java.util.HashMap;
import java.util.Map;

public class UserQuota {
    Map<String, Integer> cache = new HashMap<>();

    public void set(String user, int quotas) {
        cache.put(user, quotas);
    }

    public Integer getQuotasByUser(String user) {
        return cache.get(user);
    }

    public boolean containsUser(String user) {
        return cache.containsKey(user);
    }

    public Map<String, Integer> getCache() {
        return cache;
    }

    @Override
    public String toString() {
        return cache.toString();
    }
}
