package com.gearbox.platform.slurm.service;

import com.gearbox.core.model.Job;
import com.gearbox.core.service.JobService;
import com.gearbox.platform.slurm.util.SlurmCmdExecuteResultParser;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "system.type", havingValue = "slurm")
public class SlurmJobService implements JobService {
    @Override
    public List<Job> listWaitingJobs() {
        List<String> jobInfos = SlurmCommandExecutor.listWaitingJobInfos();
        return SlurmCmdExecuteResultParser.parseJobInfos(jobInfos);
    }

    @Override
    public List<Job> listRunningJobs() {
        List<String> jobInfos = SlurmCommandExecutor.listRunningJobInfos();
        return SlurmCmdExecuteResultParser.parseJobInfos(jobInfos);
    }
}
