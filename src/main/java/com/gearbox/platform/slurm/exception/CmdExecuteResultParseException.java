package com.gearbox.platform.slurm.exception;

public class CmdExecuteResultParseException extends RuntimeException {
    public CmdExecuteResultParseException(String message) {
        super(message);
    }
}
