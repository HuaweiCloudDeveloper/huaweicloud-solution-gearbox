package com.gearbox.core.cache;

import com.gearbox.core.configuration.SystemConfiguration;
import com.gearbox.core.configuration.TaskConfiguration;
import com.gearbox.core.model.Job;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class UserWaitingJobCache {
    private static final Logger LOG = LoggerFactory.getLogger(UserWaitingJobCache.class);

    private static final long MILLISECONDS_OF_ONE_HOUR = 3600 * 1000;

    private Map<String, Map<String, WaitingJobHolder>> waitingJobHolderMap = new ConcurrentHashMap<>();

    @Autowired
    private SystemConfiguration systemConfiguration;

    @Autowired
    private TaskConfiguration taskConfiguration;

    public void updateCache(List<Job> jobs) {
        // 更新排队任务缓存,根据Jobs生成新的缓存结构(waitingMap)，根据waitingJobHolderMap更新每个job的等待时间(aggregateWaitingTime)
        Map<String, Map<String, WaitingJobHolder>> waitingMap = new ConcurrentHashMap<>();
        jobs.forEach(job -> {
            WaitingJobHolder newWaitingJobHolder = buildNewMap(job, waitingMap);
            WaitingJobHolder oldWaitingJobHolder = GetOldWaitingJobHolder(job);
            if (oldWaitingJobHolder == null) {
                return;
            }
            newWaitingJobHolder.aggregateWaitingTime += oldWaitingJobHolder.aggregateWaitingTime;
        });

        waitingJobHolderMap = waitingMap;
    }
    public WaitingJobHolder buildNewMap(Job job,Map<String, Map<String, WaitingJobHolder>> waitingMap) {
        waitingMap.putIfAbsent(job.getUser(), new HashMap<>());
        WaitingJobHolder newHolder = new WaitingJobHolder(job);
        newHolder.updateTime = System.currentTimeMillis();
        newHolder.aggregateWaitingTime = taskConfiguration.getMetricReportPeriod();
        waitingMap.get(job.getUser()).put(generateJobMapKey(job), newHolder);
        return newHolder;
    }

    public WaitingJobHolder GetOldWaitingJobHolder(Job job) {
        Map<String, WaitingJobHolder> userMap = waitingJobHolderMap.get(job.getUser());
        if (userMap == null) {
            return null;
        }
        return userMap.get(generateJobMapKey(job));
    }

    public Set<String> getUsers() {
        return new HashSet<>(waitingJobHolderMap.keySet());
    }

    public List<Job> getJobsByUser(String user) {
        return waitingJobHolderMap.get(user)
            .values()
            .stream()
            .filter(holder -> holder.getAggregateWaitingTime() > systemConfiguration.getJobWaitTime())
            .map(WaitingJobHolder::getJob)
            .collect(Collectors.toList());
    }

    public void removeUser(String user) {
        waitingJobHolderMap.remove(user);
    }

    public void removeJobIfNotUpdateLonging(Job job) {
        Map<String, WaitingJobHolder> holderMap = waitingJobHolderMap.get(job.getUser());
        if (holderMap == null) {
            return;
        }
        WaitingJobHolder holder = holderMap.get(generateJobMapKey(job));
        if (holder == null) {
            return;
        }
        if (System.currentTimeMillis() - holder.updateTime < MILLISECONDS_OF_ONE_HOUR) {
            return;
        }
        holderMap.remove(generateJobMapKey(job));
    }

    private String generateJobMapKey(Job job) {
        return job.getId() + "_" + job.getName();
    }

    public void printAllJobs() {
        LOG.info("user waiting jobs={}", waitingJobHolderMap);
    }

    private static class WaitingJobHolder {
        private final Job job;

        private volatile int aggregateWaitingTime;

        private volatile long updateTime;

        public WaitingJobHolder(Job job) {
            this.job = job;
        }

        public Job getJob() {
            return job;
        }

        public int getAggregateWaitingTime() {
            return aggregateWaitingTime;
        }

        @Override
        public String toString() {
            return "WaitingJobHolder{" + "job=" + job + ", aggregateWaitingTime=" + aggregateWaitingTime
                + ", updateTime=" + updateTime + '}';
        }
    }
}
