package com.gearbox.core.cache;

import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class NodeIdleTimeCache {
    private final Map<String, IdleTimeHolder> idleTimeHolderMap = new ConcurrentHashMap<>();

    public void increaseIdleTime(String nodeName, int time, int maxTime) {
        IdleTimeHolder timeHolder = idleTimeHolderMap.get(nodeName);
        if (timeHolder != null && timeHolder.aggregateIdleTime > maxTime) {
            timeHolder.updateTime = System.currentTimeMillis();
            return;
        }
        increaseIdleTime(nodeName, time);
    }

    private void increaseIdleTime(String nodeName, int time) {
        idleTimeHolderMap.putIfAbsent(nodeName, new IdleTimeHolder(nodeName));
        IdleTimeHolder timeHolder = idleTimeHolderMap.get(nodeName);
        if (timeHolder == null) {
            return;
        }
        timeHolder.aggregateIdleTime += time;
        timeHolder.updateTime = System.currentTimeMillis();
    }

    public int getAggregateIdleTime(String nodeName) {
        IdleTimeHolder holder = idleTimeHolderMap.get(nodeName);
        return holder != null ? holder.aggregateIdleTime : -1;
    }

    public Set<String> getAllNodeNames() {
        return new HashSet<>(idleTimeHolderMap.keySet());
    }

    public void remove(String nodeName) {
        idleTimeHolderMap.remove(nodeName);
    }

    private static class IdleTimeHolder {
        private volatile int aggregateIdleTime;

        private volatile long updateTime;

        private final String nodeName;

        public IdleTimeHolder(String nodeName) {
            this.nodeName = nodeName;
        }

        @Override
        public String toString() {
            return "IdleTimeHolder{" + "aggregateIdleTime=" + aggregateIdleTime + ", updateTime=" + updateTime
                + ", nodeName='" + nodeName + '\'' + '}';
        }
    }
}
