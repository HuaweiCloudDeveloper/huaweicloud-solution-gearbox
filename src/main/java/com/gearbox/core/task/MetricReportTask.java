package com.gearbox.core.task;

import com.gearbox.core.driver.CesHandler;
import com.gearbox.core.metric.Metric;
import com.gearbox.core.metric.MetricCalculator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class MetricReportTask {
    private static final Logger LOG = LoggerFactory.getLogger(MetricReportTask.class);

    @Autowired
    private MetricCalculator metricCalculator;

    @Autowired
    private CesHandler cesHandler;

    @Scheduled(initialDelay = 10L, fixedDelayString = "${task.metric-report-period}", timeUnit = TimeUnit.SECONDS)
    void report() {
        try {
            LOG.info("do MetricReportTask begin");
            Metric metric = metricCalculator.calculateMetric();
            LOG.info("calculate metric success, metric={}", metric);
            cesHandler.reportMetric(metric);
            LOG.info("do MetricReportTask end");
        } catch (Throwable e) {
            LOG.error("do MetricReportTask error", e);
        }
    }
}
