package com.gearbox.core.http;

import com.gearbox.core.configuration.UserConfiguration;
import com.huaweicloud.sdk.core.auth.BasicCredentials;
import com.huaweicloud.sdk.core.http.HttpConfig;

import org.springframework.stereotype.Component;

@Component
public class HttpConfigFactory {

    private final UserConfiguration envConfig;

    public HttpConfigFactory(UserConfiguration envConfig) {
        this.envConfig = envConfig;
    }

    public HttpConfig getHttpConfig() {
        HttpConfig config = HttpConfig.getDefaultHttpConfig();
        config.withIgnoreSSLVerification(true);
        if (envConfig.getProxyAddress() != null && !envConfig.getProxyAddress().isEmpty()) {
            config.withProxyHost(envConfig.getProxyAddress())
                .withProxyPort(Integer.parseInt(envConfig.getProxyPort()))
                .withProxyUsername(envConfig.getProxyUserName())
                .withProxyPassword(envConfig.getProxyPassword());
        }
        return config;
    }

    public BasicCredentials getBasicCredentials() {
        return new BasicCredentials()
            .withAk(envConfig.getAk())
            .withSk(envConfig.getSk())
            .withProjectId(envConfig.getProjectId());
    }
}
