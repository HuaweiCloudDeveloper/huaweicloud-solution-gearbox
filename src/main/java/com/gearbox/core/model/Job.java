package com.gearbox.core.model;

public class Job {
    private String state;

    private String user;

    private String nodes;

    private String id;

    private String name;

    private int cpus;

    private int memory;

    private int taskCount;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getNodes() {
        return nodes;
    }

    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCpus() {
        return cpus;
    }

    public void setCpus(int cpus) {
        this.cpus = cpus;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    public int getTaskCount() {
        return taskCount;
    }

    public void setTaskCount(int taskCount) {
        this.taskCount = taskCount;
    }

    @Override
    public String toString() {
        return "Job{" +
            "state='" + state + '\'' +
            ", user='" + user + '\'' +
            ", nodes='" + nodes + '\'' +
            ", id='" + id + '\'' +
            ", name='" + name + '\'' +
            ", cpus=" + cpus +
            ", memory=" + memory +
            ", taskCount=" + taskCount +
            '}';
    }
}
