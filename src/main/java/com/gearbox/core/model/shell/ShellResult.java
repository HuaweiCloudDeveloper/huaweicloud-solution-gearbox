package com.gearbox.core.model.shell;

import java.util.ArrayList;
import java.util.List;

public class ShellResult {
    /**
     * 超时退出
     */
    public static final int TIMEOUT = 13;

    /**
     * 发送异常
     */
    public static final int ERROR = 1;

    /**
     * 执行成功返回值
     */
    public static final int SUCCESS = 0;

    /**
     * 脚本退出时的值
     */
    private int exitValue;

    /**
     * 脚本返回值(执行脚本后再屏幕上打印的消息)
     */
    private List<String> returnValues = new ArrayList<>();

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }

    public ShellResult withCommandLine(String commandLine) {
        this.commandLine = commandLine;
        return this;
    }

    private String commandLine;

    /**
     * 获取脚本退出时返回的值
     *
     * @return int 脚本退出时的返回值
     */
    public int getExitValue() {
        return exitValue;
    }

    /**
     * 设置脚本退出时返回的值
     *
     * @param exitValue 退出时返回的值
     */
    public void setExitValue(int exitValue) {
        this.exitValue = exitValue;
    }

    /**
     * 获取脚本返回值(执行脚本后再屏幕上打印的消息)
     *
     * @return 脚本返回值(执行脚本后再屏幕上打印的消息)
     */
    public List<String> getReturnValues() {
        return returnValues;
    }

    /**
     * 设置脚本返回值(执行脚本后再屏幕上打印的消息)
     *
     * @param returnValues 脚本返回值(执行脚本后再屏幕上打印的消息)
     */
    public void setReturnValues(List<String> returnValues) {
        this.returnValues = returnValues;
    }

    public boolean isSuccess() {
        return exitValue == SUCCESS;
    }

    @Override
    public String toString() {
        return "ShellResult{" +
            "exitValue=" + exitValue +
            ", returnValues=" + returnValues +
            ", commandLine='" + commandLine + '\'' +
            '}';
    }
}
