package com.gearbox.core.model;

import com.gearbox.core.constant.NodeStatusEnum;

public class Node {
    private String name;

    private NodeStatusEnum status;

    private boolean enabled = true;

    public Node(String name, NodeStatusEnum status) {
        this.name = name;
        this.status = status;
    }

    public Node(String name, NodeStatusEnum status, boolean isEnabled) {
        this.name = name;
        this.status = status;
        this.enabled = isEnabled;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NodeStatusEnum getStatus() {
        return status;
    }

    public void setStatus(NodeStatusEnum status) {
        this.status = status;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "Node{" +
            "name='" + name + '\'' +
            ", status=" + status +
            ", enabled=" + enabled +
            '}';
    }
}
