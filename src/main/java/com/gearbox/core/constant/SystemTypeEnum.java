package com.gearbox.core.constant;

import java.util.Locale;

public enum SystemTypeEnum {
    SLURM,
    DEADLINE;

    public static SystemTypeEnum value(String type) {
        String formatType = type.toUpperCase(Locale.ROOT);
        for (SystemTypeEnum statusEnum : values()) {
            if (formatType.contains(statusEnum.name())) {
                return statusEnum;
            }
        }
        return SLURM;
    }
}
