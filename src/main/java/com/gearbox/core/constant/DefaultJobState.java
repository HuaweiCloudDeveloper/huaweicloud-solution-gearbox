package com.gearbox.core.constant;

public enum DefaultJobState {
    QUEUED,
    RUNNING,
    COMPLETED;
}
