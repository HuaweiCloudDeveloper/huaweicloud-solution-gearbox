package com.gearbox.core.constant;

import com.gearbox.core.exception.GearboxException;

import java.util.Locale;

public enum DeadlineJobState {
    PENDING,
    QUEUED,
    ACTIVE,
    SUSPEND,
    COMPLETED;

    public static DeadlineJobState value(String state) {
        String formatState = state.toUpperCase(Locale.ROOT);
        for (DeadlineJobState jobState : values()) {
            if (formatState.contains(jobState.name())) {
                return jobState;
            }
        }
        throw new GearboxException("Deadline job state is invalid.");
    }

    public DefaultJobState toJobState() {
        switch (this) {
            case COMPLETED:
                return DefaultJobState.COMPLETED;
            case QUEUED:
                return DefaultJobState.QUEUED;
            default:
                return DefaultJobState.COMPLETED;
        }
    }
}
