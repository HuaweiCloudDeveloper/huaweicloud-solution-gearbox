package com.gearbox.core.constant;

import com.gearbox.core.exception.GearboxException;

import java.util.Locale;

public enum SlurmInstanceStatusEnum {
    DRAIN,
    MIX,
    ALLOC,
    DOWN,
    ERROR,
    IDLE;

    public static SlurmInstanceStatusEnum value(String status) {
        String formatStatus = status.toUpperCase(Locale.ROOT);
        if ((status.endsWith("*") || status.endsWith("ing")) && formatStatus.contains(DRAIN.name())) {
            return DRAIN;
        }
        if (status.endsWith("*") && formatStatus.contains(MIX.name())) {
            return MIX;
        }
        if (status.endsWith("*") && formatStatus.contains(ALLOC.name())) {
            return ALLOC;
        }
        for (SlurmInstanceStatusEnum statusEnum : values()) {
            if (formatStatus.contains(statusEnum.name())) {
                return statusEnum;
            }
        }
        throw new GearboxException("Slurm instance status is invalid.");
    }

}
