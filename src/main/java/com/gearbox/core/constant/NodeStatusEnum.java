package com.gearbox.core.constant;

public enum NodeStatusEnum {
    IDLE,
    WORKING,
    DRAIN,
    DOWN;

    public static NodeStatusEnum toNodeStatus(DeadlineInstanceStatusEnum deadlineInstanceStatusEnum) {
        // deadline没有Drain状态，通过将节点disable实现同样的效果
        switch (deadlineInstanceStatusEnum) {
            case OFFLINE:
            case STALLED:
                return NodeStatusEnum.DOWN;
            case IDLE:
                return NodeStatusEnum.IDLE;
            default:
                return NodeStatusEnum.WORKING;
        }
    }

    public static NodeStatusEnum toNodeStatus(SlurmInstanceStatusEnum slurmInstanceStatusEnum) {
        switch (slurmInstanceStatusEnum) {
            case DRAIN:
                return NodeStatusEnum.DRAIN;
            case DOWN:
            case ERROR:
                return NodeStatusEnum.DOWN;
            case IDLE:
                return NodeStatusEnum.IDLE;
            default:
                return NodeStatusEnum.WORKING;
        }
    }

    public boolean isAbnormal() {
        return this.equals(DOWN);
    }
}
