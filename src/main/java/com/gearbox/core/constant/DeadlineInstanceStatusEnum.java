package com.gearbox.core.constant;

import com.gearbox.core.exception.GearboxException;

import java.util.Locale;

public enum DeadlineInstanceStatusEnum {
    DISABLED,
    RENDERING,
    OFFLINE,
    STALLED,
    HOUSECLEANING,
    IDLE;

    public static DeadlineInstanceStatusEnum value(String status) {
        String formatStatus = status.toUpperCase(Locale.ROOT);
        for (DeadlineInstanceStatusEnum statusEnum : values()) {
            if (formatStatus.contains(statusEnum.name())) {
                return statusEnum;
            }
        }
        throw new GearboxException("Deadline instance status is invalid.");
    }
}
