package com.gearbox.core.constant;

import com.gearbox.core.exception.GearboxException;

import java.util.Locale;

public enum SlurmJobState {
    PENDING,
    RUNNING,
    COMPLETING,
    COMPLETED;

    public static SlurmJobState value(String state) {
        String formatState = state.toUpperCase(Locale.ROOT);
        for (SlurmJobState jobState : values()) {
            if (formatState.contains(jobState.name())) {
                return jobState;
            }
        }
        throw new GearboxException("Slurm job state is invalid.");
    }

    public DefaultJobState toJobState() {
        switch (this) {
            case PENDING:
                return DefaultJobState.QUEUED;
            case RUNNING:
                return DefaultJobState.RUNNING;
            default:
                return DefaultJobState.COMPLETED;
        }
    }
}
