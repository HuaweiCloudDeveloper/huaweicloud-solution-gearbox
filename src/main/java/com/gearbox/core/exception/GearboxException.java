package com.gearbox.core.exception;

public class GearboxException extends RuntimeException {
    public GearboxException(String message) {
        super(message);
    }
}
