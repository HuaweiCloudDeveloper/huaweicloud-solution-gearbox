package com.gearbox.core.exception;

public class RestApiExecuteException extends RuntimeException {
    public RestApiExecuteException(String message) {
        super(message);
    }
}
