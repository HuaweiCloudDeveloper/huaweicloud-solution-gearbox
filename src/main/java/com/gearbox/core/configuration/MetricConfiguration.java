package com.gearbox.core.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;

@Validated
@Configuration
@ConfigurationProperties(prefix = "metric")
public class MetricConfiguration {
    private String namespace;

    private String name;

    private String dimensionName;

    private String dimensionId;

    @Min(1)
    private int reportTtl;

    private String metricReportEndpoint;

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDimensionName() {
        return dimensionName;
    }

    public void setDimensionName(String dimensionName) {
        this.dimensionName = dimensionName;
    }

    public String getDimensionId() {
        return dimensionId;
    }

    public void setDimensionId(String dimensionId) {
        this.dimensionId = dimensionId;
    }

    public int getReportTtl() {
        return reportTtl;
    }

    public void setReportTtl(int reportTtl) {
        this.reportTtl = reportTtl;
    }

    public String getMetricReportEndpoint() {
        return metricReportEndpoint;
    }

    public void setMetricReportEndpoint(String metricReportEndpoint) {
        this.metricReportEndpoint = metricReportEndpoint;
    }

    @Override
    public String toString() {
        return "MetricConfiguration{" + "namespace='" + namespace + '\'' + ", name='" + name + '\''
            + ", dimensionName='" + dimensionName + '\'' + ", dimensionId='" + dimensionId + '\'' + ", metricReportTtl="
            + reportTtl + ", metricReportEndpoint='" + metricReportEndpoint + '\'' + '}';
    }
}
