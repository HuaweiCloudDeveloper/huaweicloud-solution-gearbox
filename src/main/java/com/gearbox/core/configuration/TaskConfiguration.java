package com.gearbox.core.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;

@Validated
@Configuration
@ConfigurationProperties(prefix = "task")
public class TaskConfiguration {
    @Min(1)
    private int healthAuditPeriod;

    @Min(1)
    private int metricReportPeriod;

    @Min(1)
    private int scaleInPeriod;

    @Min(1)
    private int deleteInstancePeriod;

    @Min(1)
    private int discoverInstancePeriod;

    @Min(1)
    private int diffInstanceAndNodePeriod;

    @Min(1)
    private int refreshCachePeriod;

    public int getHealthAuditPeriod() {
        return healthAuditPeriod;
    }

    public void setHealthAuditPeriod(int healthAuditPeriod) {
        this.healthAuditPeriod = healthAuditPeriod;
    }

    public int getMetricReportPeriod() {
        return metricReportPeriod;
    }

    public void setMetricReportPeriod(int metricReportPeriod) {
        this.metricReportPeriod = metricReportPeriod;
    }

    public int getScaleInPeriod() {
        return scaleInPeriod;
    }

    public void setScaleInPeriod(int scaleInPeriod) {
        this.scaleInPeriod = scaleInPeriod;
    }

    public int getDeleteInstancePeriod() {
        return deleteInstancePeriod;
    }

    public void setDeleteInstancePeriod(int deleteInstancePeriod) {
        this.deleteInstancePeriod = deleteInstancePeriod;
    }

    public int getDiscoverInstancePeriod() {
        return discoverInstancePeriod;
    }

    public void setDiscoverInstancePeriod(int discoverInstancePeriod) {
        this.discoverInstancePeriod = discoverInstancePeriod;
    }

    public int getDiffInstanceAndNodePeriod() {
        return diffInstanceAndNodePeriod;
    }

    public void setDiffInstanceAndNodePeriod(int diffInstanceAndNodePeriod) {
        this.diffInstanceAndNodePeriod = diffInstanceAndNodePeriod;
    }

    public int getRefreshCachePeriod() {
        return refreshCachePeriod;
    }

    public void setRefreshCachePeriod(int refreshCachePeriod) {
        this.refreshCachePeriod = refreshCachePeriod;
    }

    @Override
    public String toString() {
        return "TaskConfiguration{" + "healthAuditPeriod=" + healthAuditPeriod + ", metricReportPeriod="
            + metricReportPeriod + ", scaleInPeriod=" + scaleInPeriod + ", deleteInstancePeriod=" + deleteInstancePeriod
            + ", discoverInstancePeriod=" + discoverInstancePeriod + ", diffInstanceAndNodePeriod="
            + diffInstanceAndNodePeriod + ", refreshCachePeriod=" + refreshCachePeriod + '}';
    }
}
