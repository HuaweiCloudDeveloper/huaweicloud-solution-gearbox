package com.gearbox.core.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "user")
public class UserConfiguration {
    private String ak;

    private String sk;

    private String projectId;

    private String proxyAddress;

    private String proxyPort;

    private String proxyUserName;

    private String proxyPassword;

    public String getAk() {
        return ak;
    }

    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSk() {
        return sk;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getProxyAddress() {
        return proxyAddress;
    }

    public void setProxyAddress(String proxyAddress) {
        this.proxyAddress = proxyAddress;
    }

    public String getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

    public String getProxyUserName() {
        return proxyUserName;
    }

    public void setProxyUserName(String proxyUserName) {
        this.proxyUserName = proxyUserName;
    }

    public String getProxyPassword() {
        return proxyPassword;
    }

    public void setProxyPassword(String proxyPassword) {
        this.proxyPassword = proxyPassword;
    }

    @Override
    public String toString() {
        return "UserConfiguration{" + "ak='" + ak + '\'' + ", sk='" + sk + '\'' + ", projectId='" + projectId + '\''
            + ", proxyAddress='" + proxyAddress + '\'' + ", proxyPort='" + proxyPort + '\'' + ", proxyUserName='"
            + proxyUserName + '\'' + ", proxyPassword='" + proxyPassword + '\'' + '}';
    }
}
