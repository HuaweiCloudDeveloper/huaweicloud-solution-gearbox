package com.gearbox.core.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

import java.util.concurrent.Executors;

@Configuration
public class ScheduleConfig implements SchedulingConfigurer {
    /**
     * 取决于系统有多少个@Schedule标注的任务，当前共有7个任务
     */
    private static final int APPLICATION_SCHEDULED_TASK_NUMBER = 8;

    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(Executors.newScheduledThreadPool(APPLICATION_SCHEDULED_TASK_NUMBER));
    }
}
