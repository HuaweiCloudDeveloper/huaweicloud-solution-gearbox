package com.gearbox.core.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;

@Validated
@Configuration
@ConfigurationProperties(prefix = "as")
public class AsConfiguration {
    private String endpoint;

    private String group;

    @Min(1)
    private int listInstanceLimit;

    @Min(1)
    private int deleteInstanceLimit;

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public int getListInstanceLimit() {
        return listInstanceLimit;
    }

    public void setListInstanceLimit(int listInstanceLimit) {
        this.listInstanceLimit = listInstanceLimit;
    }

    public int getDeleteInstanceLimit() {
        return deleteInstanceLimit;
    }

    public void setDeleteInstanceLimit(int deleteInstanceLimit) {
        this.deleteInstanceLimit = deleteInstanceLimit;
    }

    @Override
    public String toString() {
        return "AsConfiguration{" + "endpoint='" + endpoint + '\'' + ", group='" + group + '\'' + ", listInstanceLimit="
            + listInstanceLimit + ", deleteInstanceLimit=" + deleteInstanceLimit + '}';
    }
}
