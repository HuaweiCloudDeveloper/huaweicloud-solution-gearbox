package com.gearbox.core.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import java.util.LinkedHashSet;
import java.util.Set;

import javax.validation.constraints.Min;

@Validated
@Configuration
@ConfigurationProperties(prefix = "system")
public class SystemConfiguration {
    private String type;

    private Set<String> stableNodes = new LinkedHashSet<>();

    private String stablePartition;

    private String variablePartition;

    @Min(1)
    private int scaleInTime;

    @Min(1)
    private int registerTimeoutMinutes;

    @Min(1)
    private int jobWaitTime;

    @Min(1)
    private int cpu;

    @Min(1)
    private int memory;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<String> getStableNodes() {
        return stableNodes;
    }

    public void setStableNodes(Set<String> stableNodes) {
        this.stableNodes = stableNodes;
    }

    public String getStablePartition() {
        return stablePartition;
    }

    public void setStablePartition(String stablePartition) {
        this.stablePartition = stablePartition;
    }

    public String getVariablePartition() {
        return variablePartition;
    }

    public void setVariablePartition(String variablePartition) {
        this.variablePartition = variablePartition;
    }

    public int getScaleInTime() {
        return scaleInTime;
    }

    public void setScaleInTime(int scaleInTime) {
        this.scaleInTime = scaleInTime;
    }

    public int getRegisterTimeoutMinutes() {
        return registerTimeoutMinutes;
    }

    public void setRegisterTimeoutMinutes(int registerTimeoutMinutes) {
        this.registerTimeoutMinutes = registerTimeoutMinutes;
    }

    public int getJobWaitTime() {
        return jobWaitTime;
    }

    public void setJobWaitTime(int jobWaitTime) {
        this.jobWaitTime = jobWaitTime;
    }

    public int getCpu() {
        return cpu;
    }

    public void setCpu(int cpu) {
        this.cpu = cpu;
    }

    public int getMemory() {
        return memory;
    }

    public void setMemory(int memory) {
        this.memory = memory;
    }

    @Override
    public String toString() {
        return "SystemConfiguration{" + "type='" + type + '\'' + ", stableNodes=" + stableNodes + ", stablePartition='"
            + stablePartition + '\'' + ", variablePartition='" + variablePartition + '\'' + ", scaleInIdleTime="
            + scaleInTime + ", registerTimeoutMinutes=" + registerTimeoutMinutes + ", jobWaitTime=" + jobWaitTime
            + ", cpu=" + cpu + ", memory=" + memory + '}';
    }
}
