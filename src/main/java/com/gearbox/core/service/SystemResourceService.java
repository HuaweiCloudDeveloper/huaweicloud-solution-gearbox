package com.gearbox.core.service;

public interface SystemResourceService {
    boolean isInEtcHosts(String keyword);

    void addEtcHostsRecord(String host, String ip);
}
