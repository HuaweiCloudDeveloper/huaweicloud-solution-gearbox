package com.gearbox.core.service;

import com.gearbox.core.model.Node;

import java.util.List;

public interface NodeService {
    List<Node> listUnstableNodes();

    List<Node> listUnstableDrainNodes();

    List<Node> listUnstableIdleNodes();

    // 从业务集群中删除节点，实现节点移除和配置更新
    void remove(Node node);

    // 在业务集群中设置集群排空状态
    void setToDrain(String nodeName);

    int getUserNodeQuotas(String userName);

    boolean isWorking(String nodeName);
}
