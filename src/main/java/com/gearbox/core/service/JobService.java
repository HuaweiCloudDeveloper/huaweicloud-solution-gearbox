package com.gearbox.core.service;

import com.gearbox.core.model.Job;

import java.util.List;

public interface JobService {
    List<Job> listWaitingJobs();

    List<Job> listRunningJobs();
}