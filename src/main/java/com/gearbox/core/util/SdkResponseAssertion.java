package com.gearbox.core.util;

import com.huaweicloud.sdk.core.SdkResponse;
import com.gearbox.core.exception.RestApiExecuteException;

public class SdkResponseAssertion {
    public static void assertError(SdkResponse resp, String errorMessage) {
        if (!(resp.getHttpStatusCode() >= 200 || resp.getHttpStatusCode() <= 204)) {
            throw new RestApiExecuteException(errorMessage);
        }
    }
}
