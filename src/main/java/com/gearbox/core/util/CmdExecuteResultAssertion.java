package com.gearbox.core.util;

import com.gearbox.core.exception.GearboxException;
import com.gearbox.core.model.shell.ShellResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

public class CmdExecuteResultAssertion {
    private static final Logger LOG = LoggerFactory.getLogger(CmdExecuteResultAssertion.class);

    public static void assertError(ShellResult result, String errorMessage) {
        if (!result.isSuccess()) {
            LOG.error("message: {}, detail: {}", errorMessage, result.getReturnValues());
            throw new GearboxException(errorMessage);
        }
    }

    public static void assertError(ShellResult result) {
        assertError(result,
            String.format(Locale.ROOT, "run [%s] failed", result.getCommandLine()));
    }
}
