package com.gearbox.core.metric;

import java.math.BigDecimal;

public class CustomMetric implements Metric {
    private final String name;

    private final BigDecimal value;

    public CustomMetric(String name, BigDecimal value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "CustomSlurmMetric{" +
            "name='" + name + '\'' +
            ", value=" + value +
            '}';
    }
}
