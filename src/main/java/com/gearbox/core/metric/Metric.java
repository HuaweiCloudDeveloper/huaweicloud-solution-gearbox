package com.gearbox.core.metric;

import java.math.BigDecimal;

public interface Metric {
    String getName();

    BigDecimal getValue();
}
