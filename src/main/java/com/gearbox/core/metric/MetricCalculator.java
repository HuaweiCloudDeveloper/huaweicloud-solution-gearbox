package com.gearbox.core.metric;

public interface MetricCalculator {
    Metric calculateMetric();
}
