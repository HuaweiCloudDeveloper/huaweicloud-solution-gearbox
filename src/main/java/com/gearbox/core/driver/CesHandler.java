package com.gearbox.core.driver;

import com.gearbox.core.metric.Metric;

public interface CesHandler {
    void reportMetric(Metric metric);
}
