/*
 * Copyright (c) Huawei Technologies Co., Ltd. 2021-2021. All rights reserved.
 */

package com.gearbox.core.driver;

import com.huaweicloud.sdk.as.v1.model.BatchRemoveScalingInstancesResponse;
import com.huaweicloud.sdk.as.v1.model.ScalingGroupInstance;

import java.util.List;
import java.util.Map;

public interface AsHandler {
    List<ScalingGroupInstance> listAllScalingInstance();

    Map<String, ScalingGroupInstance> getNameMapGroupInstances();

    BatchRemoveScalingInstancesResponse deleteInstances(List<String> instances);
}
