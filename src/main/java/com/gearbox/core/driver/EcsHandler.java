package com.gearbox.core.driver;

import com.huaweicloud.sdk.ecs.v2.model.ServerDetail;

public interface EcsHandler {
    ServerDetail getInstanceDetailById(String id);
}
